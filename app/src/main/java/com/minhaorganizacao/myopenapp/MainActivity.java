package com.minhaorganizacao.myopenapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void meuTeste(View view) {
        NovaThread novaThread = new NovaThread();
        novaThread.start();
    }

    class NovaThread extends Thread {
        public void run() {
//            for (int i = 0; i <= 10; i++) {
//                Log.d("teste", "contador: " + i);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
            button = findViewById(R.id.button);
            textView = findViewById(R.id.textView);

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    NewTask task = new NewTask();
                    String urlApi = "https://viacep.com.br/ws/37412484/json/";
                    task.execute(urlApi);
                }
            });
        }
    }

    class NewTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String stringUrl = strings[0];
            InputStream inputStream = null;
            InputStreamReader inputStreamReader = null;
            StringBuffer buffer = null;

            try {
                URL url = new URL(stringUrl);
                HttpURLConnection conexao = (HttpURLConnection) url.openConnection();

                inputStream = conexao.getInputStream();

                inputStreamReader = new InputStreamReader( inputStream );
                inputStreamReader.toString();
                buffer = new StringBuffer();

                BufferedReader reader = new BufferedReader(inputStreamReader);
                String linha = "";

                while((linha = reader.readLine()) != null){
                    buffer.append(linha);
                }

                Log.d("reader", "doInBackground: "+buffer);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return buffer.toString();
        }

        @Override
        protected void onPostExecute(String resultado) {
            super.onPostExecute(resultado);
            textView.setText(resultado);
        }
    }
}